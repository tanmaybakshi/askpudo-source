//
//  AvailableViewController.swift
//  AskPUDODriver
//
//  Created by Tanmay bakshi on 2016-12-22.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import Parse

var DR_request: String?
var DR_to: Coordinate?
var DR_child: String?

class AvailableViewController: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        toggleAvailability(true)
        currentAvailableView = self
    }
    
    func toggleAvailability(_ to: Bool) {
        let query = PFQuery(className: "users_driver")
        query.whereKey("UUID", equalTo: CURRENTUSER_ID)
        let objects = try! query.findObjects()
        if let currentuser = objects.first {
            currentuser["available"] = to
            try! currentuser.save()
        } else {
            //TODO: Add "impossible" logic here.
            exit(0)
        }
    }
    
    func receievedDriverRequest(request: String, to: Coordinate, childID: String) {
        DR_request = request
        DR_to = to
        DR_child = childID
        self.performSegue(withIdentifier: "rideRequested", sender: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        toggleAvailability(false)
        currentAvailableView = nil
    }
    
}
