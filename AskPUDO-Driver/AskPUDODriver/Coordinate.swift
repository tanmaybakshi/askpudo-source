//
//  Coordinate.swift
//  AskPUDODriver
//
//  Created by Tanmay bakshi on 2016-12-24.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import Foundation

struct Coordinate {
    var lat: Double
    var lon: Double
}
