//
//  GPSViewController.swift
//  AskPUDODriver
//
//  Created by Tanmay bakshi on 2016-12-24.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import Parse
import PubNub
import SKMaps
import AVFoundation

class GPSViewController: UIViewController, SKMapViewDelegate, SKRoutingDelegate, SKNavigationDelegate, CLLocationManagerDelegate, SKPositionerServiceDelegate {
    
    let locationManager = CLLocationManager()
    
    var mapView: SKMapView!
    
    var goingTo = "CHILD"
    
    var locationTimer: Timer!
    
    var client: PubNub!
    
    var session = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    var captureDevice : AVCaptureDevice?
    
    override func viewWillAppear(_ animated: Bool) {
        currentGPSView = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        currentGPSView = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locationTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { (timer) in
            self.updateLoc()
        })
        mapView = SKMapView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height))
        mapView.delegate = self
        self.view.addSubview(mapView)
        mapView.settings.rotationEnabled = false
        mapView.settings.followUserPosition = true
        mapView.settings.headingMode = SKHeadingMode.rotatingMap
        
        SKPositionerService.sharedInstance().delegate = self
        SKPositionerService.sharedInstance().startLocationUpdate()
        
        SKRoutingService.sharedInstance().routingDelegate = self
        SKRoutingService.sharedInstance().navigationDelegate = self
        SKRoutingService.sharedInstance().mapView = mapView
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
        
        let PNconfiguration = PNConfiguration(publishKey: "pub-c-86601544-548e-44ac-ae3a-1467dd32be39", subscribeKey: "sub-c-0db8705c-bcb4-11e6-9dca-02ee2ddab7fe")
        self.client = PubNub.clientWithConfiguration(PNconfiguration)
    }
    
    func updateLoc() {
        let query = PFQuery(className: "users_driver")
        query.whereKey("UUID", equalTo: CURRENTUSER_ID)
        query.findObjectsInBackground { (obj, err) in
            let currentuser = obj!.first!
            currentuser.setValue("\(self.locationManager.location!.coordinate.latitude)", forKey: "lat")
            currentuser.setValue("\(self.locationManager.location!.coordinate.longitude)", forKey: "lon")
            currentuser.saveInBackground()
        }
    }
    
    func sendPicture(to: String) {
        func convertImage(toGrayScale image: UIImage) -> UIImage {
            // Create image rectangle with current image width/height
            var imageRect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
            // Grayscale color space
            var colorSpace = CGColorSpaceCreateDeviceGray()
            // Create bitmap content with current image size and grayscale colorspace
            var context = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.none.rawValue)
            // Draw image into current context, with specified rectangle
            // using previously defined context (with grayscale colorspace)
            context!.draw(image.cgImage!, in: imageRect)
            // Create bitmap image info from pixel data in current context
            var imageRef = context?.makeImage()
            // Create a new UIImage object
            var newImage = UIImage(cgImage: imageRef!)
            // Return the new grayscale image
            return newImage
        }
        func convertImageToBase64(image: UIImage) -> String {
            var imageData = UIImagePNGRepresentation(image)
            let base64String = imageData?.base64EncodedString()
            return base64String!
        }
        func compress(image: UIImage, scaledTo newSize: CGSize) -> UIImage {
            UIGraphicsBeginImageContext(newSize)
            image.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(newSize.width), height: CGFloat(newSize.height)))
            var newImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return newImage
        }
        var frontalCamera: AVCaptureDevice!
        var allCameras = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo)
        for i in 0..<allCameras!.count {
            var camera = allCameras![i]
            if (camera as! AVCaptureDevice).position == .front {
                frontalCamera = camera as! AVCaptureDevice
            }
        }
        if frontalCamera != nil {
            session = AVCaptureSession()
            var error: Error!
            var input = try! AVCaptureDeviceInput(device: frontalCamera)
            if !(error != nil) && session.canAddInput(input) {
                session.addInput(input)
                var output = AVCaptureStillImageOutput()
                output.outputSettings = [
                    AVVideoCodecKey : AVVideoCodecJPEG
                ]
                
                if session.canAddOutput(output) {
                    session.addOutput(output)
                    var videoConnection: AVCaptureConnection? = nil
                    for connectionA in output.connections {
                        let connection = connectionA as! AVCaptureConnection
                        for porta in connection.inputPorts {
                            let port = porta as! AVCaptureInputPort
                            if port.mediaType.isEqual(AVMediaTypeVideo) {
                                videoConnection = connection
                            }
                        }
                        if videoConnection != nil {
                            
                        }
                    }
                    if videoConnection != nil {
                        session.startRunning()
                        sleep(1)
                        output.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (_ imageDataSampleBuffer: CMSampleBuffer?, _ error: Error?) in
                            if imageDataSampleBuffer != nil {
                                var imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                                var photo = UIImage(data: imageData!)!
                                self.client.publish(["parent": to, "image": convertImageToBase64(image: convertImage(toGrayScale: compress(image: photo, scaledTo: CGSize(width: 75, height: 75))))], toChannel: "ridePicResponse", withCompletion: nil)
                            }
                        })
                    }
                }
            }
        }
        //client.publish(["parent": to, "image": convertImageToBase64(image: image(from: previewLayer!))], toChannel: "ridePicResponse", withCompletion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if goingTo == "CHILD" {
            let coord = manager.location!.coordinate
            SKRoutingService.sharedInstance().routingDelegate = self
            SKRoutingService.sharedInstance().mapView = mapView
            
            let advisorSettings = SKAdvisorSettings()
            advisorSettings.advisorVoice = "en_us"
            advisorSettings.language = SKAdvisorLanguage.EN_US
            advisorSettings.advisorType = SKAdvisorType.textToSpeech
            SKRoutingService.sharedInstance().advisorConfigurationSettings = advisorSettings
            let ttsSettings = SKAdvisorTTSSettings()
            ttsSettings.rate = 0.06
            ttsSettings.pitchMultiplier = 0.8
            ttsSettings.volume = 0.9
            ttsSettings.preUtteranceDelay = 0.3
            ttsSettings.postUtteranceDelay = 0.4
            SKTTSPlayer.sharedInstance().textToSpeechConfig = ttsSettings
            
            let route = SKRouteSettings()
            route.startCoordinate = coord
            route.destinationCoordinate = childLocation!
            route.requestAdvices = true
            SKRoutingService.sharedInstance().calculateRoute(route)
            goingTo = "CHILDT"
        } else if goingTo == "" {
            let coord = manager.location!.coordinate
            SKRoutingService.sharedInstance().routingDelegate = self
            SKRoutingService.sharedInstance().mapView = mapView
            
            let advisorSettings = SKAdvisorSettings()
            advisorSettings.advisorVoice = "en_us"
            advisorSettings.language = SKAdvisorLanguage.EN_US
            advisorSettings.advisorType = SKAdvisorType.textToSpeech
            SKRoutingService.sharedInstance().advisorConfigurationSettings = advisorSettings
            let ttsSettings = SKAdvisorTTSSettings()
            ttsSettings.rate = 0.06
            ttsSettings.pitchMultiplier = 0.8
            ttsSettings.volume = 0.9
            ttsSettings.preUtteranceDelay = 0.3
            ttsSettings.postUtteranceDelay = 0.4
            SKTTSPlayer.sharedInstance().textToSpeechConfig = ttsSettings
            
            let route = SKRouteSettings()
            route.startCoordinate = coord
            route.destinationCoordinate = CLLocationCoordinate2D(latitude: DR_to!.lat, longitude: DR_to!.lon)
            route.requestAdvices = true
            SKRoutingService.sharedInstance().calculateRoute(route)
            goingTo = "D"
        }
    }
    
    func routingService(_ routingService: SKRoutingService!, didFinishRouteCalculationWithInfo routeInformation: SKRouteInformation!) {
        routingService.zoomToRoute(with: UIEdgeInsets.zero, duration: 3)
        
        let navSettings = SKNavigationSettings()
        navSettings.navigationType = SKNavigationType.real
        navSettings.distanceFormat = SKDistanceFormat.metric
        navSettings.showStreetNamePopUpsOnRoute = true
        SKRoutingService.sharedInstance().mapView!.settings.displayMode = SKMapDisplayMode.mode3D
        SKRoutingService.sharedInstance().startNavigation(with: navSettings)
    }
    
    func routingServiceDidReachDestination(_ routingService: SKRoutingService!) {
        if goingTo == "CHILDT" {
            goingTo = "T"
            locationManager.stopUpdatingLocation()
            let alert = UIAlertController(title: "AskPUDO", message: "You have reached the child. Click OK to begin navigation to final destination.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: { (alrt) in
                self.goingTo = ""
                self.locationManager.startUpdatingLocation()
            })
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        } else {
            let query = PFQuery(className: "current_rides")
            query.whereKey("driverID", equalTo: CURRENTUSER_ID)
            let res = try! query.findObjects().first!
            try! res.delete()
            self.performSegue(withIdentifier: "done", sender: self)
        }
    }
    
}
