//
//  RideRequestViewController.swift
//  AskPUDODriver
//
//  Created by Tanmay bakshi on 2016-12-24.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import PubNub
import Parse
import MapKit

var childLocation: CLLocationCoordinate2D?

class RideRequestViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet var timerLabel: UILabel!
    
    @IBOutlet var childName: UILabel!
    @IBOutlet var parentName: UILabel!
    
    @IBOutlet var mapView: MKMapView!
    
    var timer: Timer!
    var timerValue = 10
    
    var client: PubNub!
    
    override func viewDidLoad() {
        let PNconfiguration = PNConfiguration(publishKey: "pub-c-86601544-548e-44ac-ae3a-1467dd32be39", subscribeKey: "sub-c-0db8705c-bcb4-11e6-9dca-02ee2ddab7fe")
        self.client = PubNub.clientWithConfiguration(PNconfiguration)
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (t) in
            self.timerTick()
        })
        let childQuery = PFQuery(className: "users_child")
        childQuery.whereKey("UUID", equalTo: DR_child!)
        let childResult = try! childQuery.findObjects().first!
        let parentQuery = PFQuery(className: "users_parent")
        parentQuery.whereKey("childID", equalTo: DR_child!)
        let parentResult = try! parentQuery.findObjects().first!
        childName.text = childResult.value(forKey: "fullname") as! String
        parentName.text = parentResult.value(forKey: "fullname") as! String
        mapView.delegate = self
        centerMapOnLocation(location: CLLocationCoordinate2D(latitude: DR_to!.lat, longitude: DR_to!.lon))
        addPinToMap(location: CLLocationCoordinate2D(latitude: DR_to!.lat, longitude: DR_to!.lon), title: "Destination")
        let childLat = Double(childResult.value(forKey: "lat") as! String)!
        let childLon = Double(childResult.value(forKey: "lon") as! String)!
        addPinToMap(location: CLLocationCoordinate2D(latitude: childLat, longitude: childLon), title: "Child")
        childLocation = CLLocationCoordinate2D(latitude: childLat, longitude: childLon)
    }
    
    @IBAction func accept() {
        client.publish(["request": DR_request!, "childID": DR_child!, "driverID": CURRENTUSER_ID, "accepted": true], toChannel: "driverResponse", compressed: false) { (status) in
            self.initiateGPS()
            let childQuery = PFQuery(className: "users_child")
            childQuery.whereKey("UUID", equalTo: DR_child!)
            let child = try! childQuery.findObjects().first!
            let fmLat = child.value(forKey: "lat") as! String
            let fmLon = child.value(forKey: "lon") as! String
            let object = PFObject(className: "current_rides")
            object.setObject(DR_child!, forKey: "childID")
            object.setObject(CURRENTUSER_ID, forKey: "driverID")
            object.setObject("\(fmLat)", forKey: "fmLat")
            object.setObject("\(fmLon)", forKey: "fmLon")
            object.setObject("\(DR_to!.lat)", forKey: "toLat")
            object.setObject("\(DR_to!.lon)", forKey: "toLon")
            try! object.save()
        }
    }
    
    @IBAction func reject() {
        client.publish(["request": DR_request!, "childID": DR_child!, "driverID": CURRENTUSER_ID, "accepted": false], toChannel: "driverResponse", compressed: false) { (status) in
            self.exitRequest()
        }
    }
    
    func initiateGPS() {
        self.performSegue(withIdentifier: "accepted", sender: self)
    }
    
    func exitRequest() {
        self.performSegue(withIdentifier: "available", sender: self)
    }
    
    func timerTick() {
        timerValue -= 1
        timerLabel.text = "\(timerValue)"
        if timerValue == 0 {
            timer.invalidate()
            exitRequest()
        }
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addPinToMap(location: CLLocationCoordinate2D, title: String) {
        let annotation = MKPointAnnotation()
        let centerCoordinate = location
        annotation.coordinate = centerCoordinate
        annotation.title = title
        mapView.addAnnotation(annotation)
    }
    
}
