//
//  ViewController.swift
//  AskPUDODriver
//
//  Created by Tanmay Bakshi on 2016-12-03.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import BRYXBanner
import Parse

class ViewControllerDriver: UIViewController {

    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func login() {
        let query = PFQuery(className: "users_driver")
        query.whereKey("username", equalTo: usernameField.text!)
        let objects = try! query.findObjects()
        if let object = objects.first {
            if (object.value(forKey: "password") as! String) == passwordField.text! {
                CURRENTUSER_ID = object.value(forKey: "UUID")! as! String
                self.performSegue(withIdentifier: "donelogin", sender: self)
            } else {
                passwordIncorrect()
            }
        } else {
            usernameNotFound()
        }
    }
    
    // Login error
    func usernameNotFound() {
        let banner = Banner(title: "Error", subtitle: "Sorry, this username isn't registered with AskPUDO Parent.", image: nil, backgroundColor: BannerColor.red)
        banner.dismissesOnTap = true
        banner.show(duration: 4.0)
    }
    
    // Login error
    func passwordIncorrect() {
        let banner = Banner(title: "Error", subtitle: "Sorry, this password is incorrect.", image: nil, backgroundColor: BannerColor.red)
        banner.dismissesOnTap = true
        banner.show(duration: 4.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

