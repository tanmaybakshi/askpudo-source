//
//  AppDelegate.swift
//  AskPUDOParent
//
//  Created by Tanmay Bakshi on 2016-11-28.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import Parse
import PubNub

var IP = ""

extension NetworkStatus {
    var isReachable: Bool {
        return self == .ReachableViaWiFi || self == .ReachableViaWWAN
    }
}

extension Date {
    
    var toString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: self)
    }
    
}

extension String {
    
    var toDate: Date {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        return dateFormat.date(from: self)!
    }
    
}

protocol childRequestDelegate {
    
    func showRequestScreen()
    
}

var currentRequestHandler: childRequestDelegate?
var currentMyChildView: MyChildViewController?

var CR_to = CLLocationCoordinate2D()

var myChildID = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PNObjectEventListener {

    var window: UIWindow?

    private var reachability:Reachability!

    var client: PubNub!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let configuration = ParseClientConfiguration {
            $0.applicationId = "FfWpbju8UEEiAm2GRNkUqzdwYg6Rk9Ny514SEXcz"
            $0.clientKey = "ddaDtNgxeW81FYs8UIG8vM15X13Y6qqVti4omnhv"
            $0.server = "https://parseapi.back4app.com"
        }
        Parse.initialize(with: configuration)
        
//        NotificationCenter.default.addObserver(self, selector:#selector(AppDelegate.checkForReachability), name: NSNotification.Name.reachabilityChanged, object: nil)
//        
//        self.reachability = Reachability.forInternetConnection()
//        self.reachability.startNotifier()
//        self.reachability.reachableOnWWAN = true
//        
//        checkReach()
        
        let PNconfiguration = PNConfiguration(publishKey: "pub-c-86601544-548e-44ac-ae3a-1467dd32be39", subscribeKey: "sub-c-0db8705c-bcb4-11e6-9dca-02ee2ddab7fe")
        self.client = PubNub.clientWithConfiguration(PNconfiguration)
        self.client.addListener(self)
        self.client.subscribeToChannels(["childRequest", "ridePicResponse"], withPresence: false)
        
        return true
    }
    
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
//        if let availableView = currentAvailableView {
//            let receieved = message.data.message! as! NSDictionary
//            if (receieved.value(forKey: "driver") as! String) == CURRENTUSER_ID {
//                let request = receieved.value(forKey: "request") as! String
//                let child = receieved.value(forKey: "child") as! String
//                let toLat = Double(receieved.value(forKey: "toLat") as! String)!
//                let toLon = Double(receieved.value(forKey: "toLon") as! String)!
//                let toCoord = Coordinate(lat: toLat, lon: toLon)
//                availableView.receievedDriverRequest(request: request, to: toCoord, childID: child)
//            }
//        }
        if message.data.channel == "childRequest" {
            let receieved = message.data.message! as! NSDictionary
            let myChildRequest = PFQuery(className: "users_parent")
            myChildRequest.whereKey("UUID", equalTo: CURRENTUSER_ID)
            let res = try! myChildRequest.findObjects().first!
            myChildID = res.value(forKey: "childID") as! String
            if (receieved.value(forKey: "child") as! String) == myChildID {
                if let requestHandler = currentRequestHandler {
                    let toLat = Double(receieved.value(forKey: "toLat") as! String)!
                    let toLon = Double(receieved.value(forKey: "toLon") as! String)!
                    CR_to = CLLocationCoordinate2D(latitude: toLat, longitude: toLon)
                    requestHandler.showRequestScreen()
                }
            }
        } else if message.data.channel == "ridePicResponse" {
            func convertBase64ToImage(base64String: String) -> UIImage {
                
                let decodedData = NSData(base64Encoded: base64String, options: NSData.Base64DecodingOptions(rawValue: 0) )
                
                var decodedimage = UIImage(data: decodedData! as Data)
                
                return decodedimage!
                
            }
            let receieved = message.data.message! as! NSDictionary
            if receieved.value(forKey: "parent") as! String == CURRENTUSER_ID {
                if let myChildViewController = currentMyChildView {
                    IMAGE_RESPONSE = convertBase64ToImage(base64String: receieved.value(forKey: "image") as! String)
                    myChildViewController.receivedPicture()
                }
            }
        }
    }
    
//    @objc func checkForReachability(notification:NSNotification) {
//        let networkReachability = notification.object as! Reachability;
//        let remoteHostStatus = networkReachability.currentReachabilityStatus()
//        if remoteHostStatus.isReachable {
//            //TODO: Implement server and handle IP changes.
//            IP = currentPublicIP()
//            if CURRENTUSER_ID != "" {
//                let query = PFQuery(className: "users_parent")
//                query.whereKey("UUID", equalTo: CURRENTUSER_ID)
//                let objects = try! query.findObjects()
//                if let object = objects.first {
//                    object.setObject(IP, forKey: "ip")
//                    object.saveInBackground()
//                }
//            }
//        }
//    }
//    
//    func checkReach() {
//        let networkReachability = reachability!
//        let remoteHostStatus = networkReachability.currentReachabilityStatus()
//        if remoteHostStatus.isReachable {
//            //TODO: Implement server and handle IP changes.
//            IP = currentPublicIP()
//        }
//    }
//    
//    func currentPublicIP() -> String {
//        let requestURLString = "https://httpbin.org/get"
//        let requestURL = URL(string: requestURLString)!
//        let responseData = try! Data(contentsOf: requestURL)
//        let responseJSON = try! JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as! [String: AnyObject]
//        return responseJSON["origin"] as! String
//    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

