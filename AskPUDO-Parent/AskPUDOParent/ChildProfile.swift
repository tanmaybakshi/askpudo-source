//
//  ChildProfile.swift
//  AskPUDOParent
//
//  Created by Tanmay bakshi on 2016-12-25.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit

class ChildProfileViewController: UIViewController, childRequestDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        currentRequestHandler = self
    }
    
    func showRequestScreen() {
        self.performSegue(withIdentifier: "childRequest", sender: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        currentRequestHandler = nil
    }
    
}
