//
//  ChildRequestViewController.swift
//  AskPUDOParent
//
//  Created by Tanmay bakshi on 2016-12-25.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import MapKit
import Parse
import PubNub

class ChildRequestViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet var mapView: MKMapView!
    
    var client: PubNub!
    
    override func viewDidLoad() {
        let PNconfiguration = PNConfiguration(publishKey: "pub-c-86601544-548e-44ac-ae3a-1467dd32be39", subscribeKey: "sub-c-0db8705c-bcb4-11e6-9dca-02ee2ddab7fe")
        self.client = PubNub.clientWithConfiguration(PNconfiguration)
        let childQuery = PFQuery(className: "users_child")
        childQuery.whereKey("UUID", equalTo: myChildID)
        let childResult = try! childQuery.findObjects().first!
        mapView.delegate = self
        centerMapOnLocation(location: CLLocationCoordinate2D(latitude: CR_to.latitude, longitude: CR_to.longitude))
        addPinToMap(location: CLLocationCoordinate2D(latitude: CR_to.latitude, longitude: CR_to.longitude), title: "Destination")
        let childLat = Double(childResult.value(forKey: "lat") as! String)!
        let childLon = Double(childResult.value(forKey: "lon") as! String)!
        addPinToMap(location: CLLocationCoordinate2D(latitude: childLat, longitude: childLon), title: "Child")
    }
    
    @IBAction func accept() {
        client.publish(["child": myChildID, "accepted": true], toChannel: "childResponse", withCompletion: nil)
        requestRide(address: CR_to)
        self.performSegue(withIdentifier: "done", sender: self)
    }
    
    func requestRide(address: CLLocationCoordinate2D) {
        let parentQuery = PFQuery(className: "users_parent")
        parentQuery.whereKey("UUID", equalTo: CURRENTUSER_ID)
        let parentResult = try! parentQuery.findObjects().first!
        let requestURLString = "http://192.168.0.14:3000/api/rideRequest?childId=\(parentResult.value(forKey: "childID") as! String)&parentId=\(CURRENTUSER_ID)&toLat=\(Double(address.latitude))&toLon=\(Double(address.longitude))"
        let requestURL = NSURL(string: requestURLString)!
        _ = NSData(contentsOf: requestURL as URL)!
    }
    
    @IBAction func reject() {
        client.publish(["child": myChildID, "accepted": true], toChannel: "childResponse", withCompletion: nil)
        self.performSegue(withIdentifier: "done", sender: self)
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addPinToMap(location: CLLocationCoordinate2D, title: String) {
        let annotation = MKPointAnnotation()
        let centerCoordinate = location
        annotation.coordinate = centerCoordinate
        annotation.title = title
        mapView.addAnnotation(annotation)
    }
    
}
