//
//  MyChildViewController.swift
//  AskPUDOParent
//
//  Created by Tanmay bakshi on 2016-12-24.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import Parse
import MapKit
import PubNub

class MyChildViewController: UIViewController, childRequestDelegate, MKMapViewDelegate {
    
    @IBOutlet var childFullName: UILabel!
    
    @IBOutlet var driverName: UILabel!
    @IBOutlet var carInfo: UILabel!
    
    @IBOutlet var ridePic: UIButton!
    
    @IBOutlet var mapView: MKMapView!
    
    var locationUpdater: Timer!
    
    var client: PubNub!
    
    var currentdriverid = ""
    
    override func viewWillAppear(_ animated: Bool) {
        currentRequestHandler = self
        currentMyChildView = self
    }
    
    override func viewDidLoad() {
        updateLabels()
        updateLocation()
        if #available(iOS 10.0, *) {
            locationUpdater = Timer.scheduledTimer(withTimeInterval: 4, repeats: true, block: { (timer) in
                self.updateLocation()
                self.updateLabels()
            })
        } else {
            // Fallback on earlier versions
        }
        let PNconfiguration = PNConfiguration(publishKey: "pub-c-86601544-548e-44ac-ae3a-1467dd32be39", subscribeKey: "sub-c-0db8705c-bcb4-11e6-9dca-02ee2ddab7fe")
        self.client = PubNub.clientWithConfiguration(PNconfiguration)
    }
    
    func showRequestScreen() {
        self.performSegue(withIdentifier: "childRequest", sender: self)
    }
    
    func updateLocation() {
        mapView.removeAnnotations(mapView.annotations)
        let childQuery = PFQuery(className: "users_child")
        childQuery.whereKey("parentID", equalTo: CURRENTUSER_ID)
        let childResult = try! childQuery.findObjects().first!
        let childLat = childResult.value(forKey: "lat") as! String
        let childLon = childResult.value(forKey: "lon") as! String
        let childLocation = CLLocationCoordinate2D(latitude: Double(childLat)!, longitude: Double(childLon)!)
        centerMapOnLocation(location: childLocation)
        addPinToMap(location: childLocation, title: "Child")
        let childID = childResult.value(forKey: "UUID") as! String
        let rideRequest = PFQuery(className: "current_rides")
        rideRequest.whereKey("childID", equalTo: childID)
        let result = try! rideRequest.findObjects()
        if let currentRide = result.first {
            let driverQuery = PFQuery(className: "users_driver")
            let driverID = currentRide.value(forKey: "driverID") as! String
            driverQuery.whereKey("UUID", equalTo: driverID)
            let result = try! driverQuery.findObjects().first!
            let driverLat = result.value(forKey: "lat") as! String
            let driverLon = result.value(forKey: "lon") as! String
            let driverLocation = CLLocationCoordinate2D(latitude: Double(driverLat)!, longitude: Double(driverLon)!)
            addPinToMap(location: driverLocation, title: "Driver")
            currentdriverid = driverID
            ridePic.isHidden = false
        } else {
            ridePic.isHidden = true
        }
    }
    
    func updateLabels() {
        let childQuery = PFQuery(className: "users_child")
        childQuery.whereKey("parentID", equalTo: CURRENTUSER_ID)
        let childResult = try! childQuery.findObjects().first!
        childFullName.text = childResult.value(forKey: "fullname") as! String
        let query = PFQuery(className: "current_rides")
        let myChildRequest = PFQuery(className: "users_parent")
        myChildRequest.whereKey("UUID", equalTo: CURRENTUSER_ID)
        let res = try! myChildRequest.findObjects().first!
        let myChildID = res.value(forKey: "childID") as! String
        query.whereKey("childID", equalTo: myChildID)
        let result = try! query.findObjects()
        if let res = result.first {
            let id = res.value(forKey: "driverID") as! String
            let driverQuery = PFQuery(className: "users_driver")
            driverQuery.whereKey("UUID", equalTo: id)
            let driverResult = try! driverQuery.findObjects().first!
            driverName.text = (driverResult.value(forKey: "fullname") as! String)
            carInfo.text = (driverResult.value(forKey: "plate") as! String)
            driverName.isHidden = false
            carInfo.isHidden = false
        } else {
            driverName.isHidden = true
            carInfo.isHidden = true
        }
    }
    
    @IBAction func sendRidePicRequest() {
        client.publish(["driver": currentdriverid, "parent": CURRENTUSER_ID], toChannel: "ridePicRequest", withCompletion: nil)
    }
    
    func receivedPicture() {
        self.performSegue(withIdentifier: "gotPic", sender: self)
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addPinToMap(location: CLLocationCoordinate2D, title: String) {
        let annotation = MKPointAnnotation()
        let centerCoordinate = location
        annotation.coordinate = centerCoordinate
        annotation.title = title
        mapView.addAnnotation(annotation)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        currentRequestHandler = nil
        currentMyChildView = nil
        locationUpdater.invalidate()
    }
    
}
