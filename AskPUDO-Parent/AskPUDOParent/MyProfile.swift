//
//  MyProfile.swift
//  AskPUDOParent
//
//  Created by Tanmay Bakshi on 2016-11-30.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController, childRequestDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        currentRequestHandler = self
    }
    
    func showRequestScreen() {
        self.performSegue(withIdentifier: "childRequest", sender: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        currentRequestHandler = nil
    }
    
}
