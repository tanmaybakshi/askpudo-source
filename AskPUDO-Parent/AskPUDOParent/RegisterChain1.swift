//
//  RegisterChain1.swift
//  AskPUDOParent
//
//  Created by Tanmay Bakshi on 2016-11-29.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit

var REGISTER_FULLNAME = ""
var REGISTER_GENDER = ""
var REGISTER_DOB = ""

class RegisterChain1: UIViewController {
    
    @IBOutlet var fullnameField: UITextField!
    @IBOutlet var genderField: UITextField!
    @IBOutlet var dobPicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func next() {
        if genderField.text! != "M" {
            if genderField.text! != "F" {
                genderNotMF()
                return
            }
        }
        REGISTER_FULLNAME = fullnameField.text!
        REGISTER_GENDER = genderField.text!
        REGISTER_DOB = dobPicker.date.toString
        self.performSegue(withIdentifier: "registerchain2", sender: self)
    }
    
    // Gender error
    func genderNotMF() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
