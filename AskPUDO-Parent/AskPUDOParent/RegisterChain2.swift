//
//  RegisterChain2.swift
//  AskPUDOParent
//
//  Created by Tanmay Bakshi on 2016-11-30.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit

var REGISTER_UNIT = ""
var REGISTER_STREET = ""
var REGISTER_CITY = ""
var REGISTER_PROV = ""
var REGISTER_POSTAL = ""
var REGISTER_COUNTRY = ""

class RegisterChain2: UIViewController {

    @IBOutlet var unit: UITextField!
    @IBOutlet var street: UITextField!
    @IBOutlet var city: UITextField!
    @IBOutlet var prov: UITextField!
    @IBOutlet var postal: UITextField!
    @IBOutlet var country: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func next() {
        REGISTER_UNIT = unit.text!
        REGISTER_STREET = street.text!
        REGISTER_CITY = city.text!
        REGISTER_PROV = prov.text!
        REGISTER_POSTAL = postal.text!
        REGISTER_COUNTRY = country.text!
        self.performSegue(withIdentifier: "registerchain3", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
