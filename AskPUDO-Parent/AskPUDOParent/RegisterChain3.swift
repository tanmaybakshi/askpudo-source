//
//  RegisterChain3.swift
//  AskPUDOParent
//
//  Created by Tanmay Bakshi on 2016-11-30.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import Parse

var CREGISTER_USERNAME = ""
var CREGISTER_PASSWORD = ""

class RegisterChain3: UIViewController {
    
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func next() {
        let query = PFQuery(className: "users_child")
        query.whereKey("username", equalTo: usernameField.text!)
        var objects = try! query.findObjects()
        if let _ = objects.first {
            usernameExists()
        } else {
            CREGISTER_USERNAME = usernameField.text!
            CREGISTER_PASSWORD = passwordField.text!
            self.performSegue(withIdentifier: "registerchain4", sender: self)
        }
    }
    
    // Register error
    func usernameExists() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
