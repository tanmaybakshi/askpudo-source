//
//  RegisterChain4.swift
//  AskPUDOParent
//
//  Created by Tanmay Bakshi on 2016-11-30.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit

var CREGISTER_FULLNAME = ""
var CREGISTER_GENDER = ""
var CREGISTER_DOB = ""

class RegisterChain4: UIViewController {

    @IBOutlet var fullnameField: UITextField!
    @IBOutlet var genderField: UITextField!
    @IBOutlet var dobPicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func next() {
        if genderField.text! != "M" {
            if genderField.text! != "F" {
                genderNotMF()
                return
            }
        }
        CREGISTER_FULLNAME = fullnameField.text!
        CREGISTER_GENDER = genderField.text!
        CREGISTER_DOB = dobPicker.date.toString
        self.performSegue(withIdentifier: "registerchain5", sender: self)
    }
    
    // Gender error
    func genderNotMF() {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
