//
//  RegisterChain5.swift
//  AskPUDOParent
//
//  Created by Tanmay Bakshi on 2016-11-30.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import Parse

var CREGISTER_UNIT = ""
var CREGISTER_STREET = ""
var CREGISTER_CITY = ""
var CREGISTER_PROV = ""
var CREGISTER_POSTAL = ""
var CREGISTER_COUNTRY = ""

class RegisterChain5: UIViewController {
    
    @IBOutlet var unit: UITextField!
    @IBOutlet var street: UITextField!
    @IBOutlet var city: UITextField!
    @IBOutlet var prov: UITextField!
    @IBOutlet var postal: UITextField!
    @IBOutlet var country: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func finish() {
        CREGISTER_UNIT = unit.text!
        CREGISTER_STREET = street.text!
        CREGISTER_CITY = city.text!
        CREGISTER_PROV = prov.text!
        CREGISTER_POSTAL = postal.text!
        CREGISTER_COUNTRY = country.text!
        
        let parentObject = PFObject(className: "users_parent")
        let childObject = PFObject(className: "users_child")
        
        let parentID = UUID().uuidString
        
        parentObject.setObject("\(REGISTER_UNIT)-*-\(REGISTER_STREET)-*-\(REGISTER_CITY)-*-\(REGISTER_PROV)-*-\(REGISTER_POSTAL)-*-\(REGISTER_COUNTRY)", forKey: "address")
        parentObject.setObject("\(REGISTER_FULLNAME)", forKey: "fullname")
        parentObject.setObject("\(REGISTER_GENDER)", forKey: "gender")
        parentObject.setObject("\(REGISTER_DOB)", forKey: "dob")
        parentObject.setObject("\(REGISTER_USERNAME)", forKey: "username")
        parentObject.setObject("\(REGISTER_PASSWORD)", forKey: "password")
        parentObject.setObject("\(parentID)", forKey: "UUID")
        
        childObject.setObject("\(CREGISTER_UNIT)-*-\(CREGISTER_STREET)-*-\(CREGISTER_CITY)-*-\(CREGISTER_PROV)-*-\(CREGISTER_POSTAL)-*-\(CREGISTER_COUNTRY)", forKey: "address")
        childObject.setObject("\(CREGISTER_FULLNAME)", forKey: "fullname")
        childObject.setObject("\(CREGISTER_GENDER)", forKey: "gender")
        childObject.setObject("\(CREGISTER_DOB)", forKey: "dob")
        childObject.setObject("\(CREGISTER_USERNAME)", forKey: "username")
        childObject.setObject("\(CREGISTER_PASSWORD)", forKey: "password")
        childObject.setObject("\(UUID().uuidString)", forKey: "UUID")
        
        CURRENTUSER_ID = parentID
        
        try! parentObject.save()
        try! childObject.save()
        
        self.performSegue(withIdentifier: "toMain", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
