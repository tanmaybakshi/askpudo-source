//
//  RequestRideViewController.swift
//  AskPUDOParent
//
//  Created by Tanmay bakshi on 2016-12-24.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import PubNub
import MapKit
import Parse

class RequestRideViewController: UIViewController {
    
    @IBOutlet var address: UITextField!
    
    @IBAction func request() {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address.text!, completionHandler: {(placemarks, error) -> Void in
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                self.requestRide(address: coordinates)
            }
        })
    }
    
    func requestRide(address: CLLocationCoordinate2D) {
        let parentQuery = PFQuery(className: "users_parent")
        parentQuery.whereKey("UUID", equalTo: CURRENTUSER_ID)
        let parentResult = try! parentQuery.findObjects().first!
        let requestURLString = "http://192.168.0.14:3000/api/rideRequest?childId=\(parentResult.value(forKey: "childID") as! String)&parentId=\(CURRENTUSER_ID)&toLat=\(Double(address.latitude))&toLon=\(Double(address.longitude))"
        let requestURL = NSURL(string: requestURLString)!
        _ = NSData(contentsOf: requestURL as URL)!
        self.performSegue(withIdentifier: "requested", sender: self)
    }
    
}
