//
//  GotPictureViewController.swift
//  AskPUDOParent
//
//  Created by Tanmay bakshi on 2016-12-26.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit

var IMAGE_RESPONSE = UIImage()

class GotPictureViewController: UIViewController {

    @IBOutlet var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.image = IMAGE_RESPONSE
    }

}
