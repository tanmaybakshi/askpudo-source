//
//  AppDelegate.swift
//  AskPUDOChild
//
//  Created by Tanmay Bakshi on 2016-12-03.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import PubNub
import Parse

protocol gotRideDelegate {
    
    func showRideScreen()
    
}

var currentRideHandler: gotRideDelegate?

var PR_driver = ""
var PR_to = CLLocationCoordinate2D()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PNObjectEventListener {

    var window: UIWindow?

    var client: PubNub!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let configuration = ParseClientConfiguration {
            $0.applicationId = "FfWpbju8UEEiAm2GRNkUqzdwYg6Rk9Ny514SEXcz"
            $0.clientKey = "ddaDtNgxeW81FYs8UIG8vM15X13Y6qqVti4omnhv"
            $0.server = "https://parseapi.back4app.com"
        }
        Parse.initialize(with: configuration)
        let PNconfiguration = PNConfiguration(publishKey: "pub-c-86601544-548e-44ac-ae3a-1467dd32be39", subscribeKey: "sub-c-0db8705c-bcb4-11e6-9dca-02ee2ddab7fe")
        self.client = PubNub.clientWithConfiguration(PNconfiguration)
        self.client.addListener(self)
        self.client.subscribeToChannels(["parentResponse"], withPresence: false)
        return true
    }
    
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        print(CURRENTUSER_ID)
        print(currentRideHandler)
        let receieved = message.data.message! as! NSDictionary
        if (receieved.value(forKey: "child") as! String) == CURRENTUSER_ID {
            if let rideHandler = currentRideHandler {
                let lat = receieved.value(forKey: "toLat") as! String
                let lon = receieved.value(forKey: "toLon") as! String
                let loc = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(lon)!)
                let driver = receieved.value(forKey: "driver") as! String
                PR_driver = driver
                PR_to = loc
                rideHandler.showRideScreen()
            }
        }
        print(receieved)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

