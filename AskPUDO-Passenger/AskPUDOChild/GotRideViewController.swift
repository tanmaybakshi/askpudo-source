//
//  GotRideViewController.swift
//  AskPUDOChild
//
//  Created by Tanmay bakshi on 2016-12-25.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import MapKit
import Parse

class GotRideViewController: UIViewController, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    
    @IBOutlet var addressLabel: UILabel!
    
    var locationTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationTimer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true, block: { (timer) in
            self.updateLoc()
        })
        let loc = CLLocation(latitude: PR_to.latitude, longitude: PR_to.longitude)
        CLGeocoder().reverseGeocodeLocation(loc) { (placemarks, err) in
            let placemark = placemarks!.first!
            DispatchQueue.global(qos: .default).sync {
                self.addressLabel.text = (placemark.addressDictionary!["FormattedAddressLines"] as! NSArray).componentsJoined(by: " ")
            }
        }
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
    }
    
    func updateLoc() {
        let query = PFQuery(className: "users_child")
        query.whereKey("UUID", equalTo: CURRENTUSER_ID)
        let currentuser = try! query.findObjects().first!
        currentuser.setValue("\(locationManager.location!.coordinate.latitude)", forKey: "lat")
        currentuser.setValue("\(locationManager.location!.coordinate.longitude)", forKey: "lon")
        currentuser.saveInBackground()
    }

}
