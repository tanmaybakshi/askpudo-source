//
//  MainViewController.swift
//  AskPUDOChild
//
//  Created by Tanmay bakshi on 2016-12-25.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import MapKit
import Parse

class MainViewController: UIViewController, gotRideDelegate, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var locationTimer: Timer!
    
    override func viewDidLoad() {
        locationTimer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true, block: { (timer) in
            self.updateLoc()
        })
        currentRideHandler = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
    }
    
    func showRideScreen() {
        self.performSegue(withIdentifier: "gotRide", sender: self)
    }
    
    func updateLoc() {
        let query = PFQuery(className: "users_child")
        query.whereKey("UUID", equalTo: CURRENTUSER_ID)
        let currentuser = try! query.findObjects().first!
        currentuser.setValue("\(locationManager.location!.coordinate.latitude)", forKey: "lat")
        currentuser.setValue("\(locationManager.location!.coordinate.longitude)", forKey: "lon")
        currentuser.saveInBackground()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        currentRideHandler = nil
    }
    
}
