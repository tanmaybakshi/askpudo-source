//
//  RequestRideViewController.swift
//  AskPUDOChild
//
//  Created by Tanmay bakshi on 2016-12-25.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import PubNub
import MapKit

class RequestRideViewController: UIViewController {

    @IBOutlet var address: UITextView!
    
    var client: PubNub!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let PNconfiguration = PNConfiguration(publishKey: "pub-c-86601544-548e-44ac-ae3a-1467dd32be39", subscribeKey: "sub-c-0db8705c-bcb4-11e6-9dca-02ee2ddab7fe")
        self.client = PubNub.clientWithConfiguration(PNconfiguration)
    }
    
    @IBAction func ride() {
        CLGeocoder().geocodeAddressString(address.text!) { (placemarks, err) in
            let placemark = placemarks!.first!
            self.client.publish(["child": CURRENTUSER_ID, "toLat": "\(placemark.location!.coordinate.latitude)", "toLon": "\(placemark.location!.coordinate.longitude)"], toChannel: "childRequest", withCompletion: nil)
        }
        self.performSegue(withIdentifier: "asked", sender: self)
    }

}
