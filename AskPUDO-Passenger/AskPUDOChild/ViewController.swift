//
//  ViewController.swift
//  AskPUDOChild
//
//  Created by Tanmay Bakshi on 2016-12-03.
//  Copyright © 2016 Tanmay Bakshi. All rights reserved.
//

import UIKit
import Parse
import BRYXBanner

var CURRENTUSER_ID = ""

struct BannerColor {
    static let red = UIColor(red:198.0/255.0, green:26.00/255.0, blue:27.0/255.0, alpha:1.000)
    static let green = UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.5/255.0, alpha:1.000)
    static let yellow = UIColor(red:255.0/255.0, green:204.0/255.0, blue:51.0/255.0, alpha:1.000)
    static let blue = UIColor(red:31.0/255.0, green:136.0/255.0, blue:255.0/255.0, alpha:1.000)
}

class ViewController: UIViewController {

    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func login() {
        let query = PFQuery(className: "users_child")
        query.whereKey("username", equalTo: usernameField.text!)
        let objects = try! query.findObjects()
        if let object = objects.first {
            if (object.value(forKey: "password") as! String) == passwordField.text! {
                CURRENTUSER_ID = object.value(forKey: "UUID")! as! String
                self.performSegue(withIdentifier: "donelogin", sender: self)
            } else {
                passwordIncorrect()
            }
        } else {
            usernameNotFound()
        }
    }
    
    // Login error
    func usernameNotFound() {
        let banner = Banner(title: "Error", subtitle: "Sorry, this username isn't registered with AskPUDO Child.", image: nil, backgroundColor: BannerColor.red)
        banner.dismissesOnTap = true
        banner.show(duration: 4.0)
    }
    
    // Login error
    func passwordIncorrect() {
        let banner = Banner(title: "Error", subtitle: "Sorry, this password is incorrect.", image: nil, backgroundColor: BannerColor.red)
        banner.dismissesOnTap = true
        banner.show(duration: 4.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

