import Parse from 'parse/node';
import {Child, Driver, DriveRequest} from '../model/index';
import request from "request-promise";
import _ from 'lodash';
import config from 'config3';
import P from 'bluebird';

const pubnub = require("pubnub")({
    publish_key   : config.pubnub.publish_key,
    subscribe_key : config.pubnub.subscribe_key
});

async function geteta(lat1, lon1, lat2, lon2) {
    const APIKey = "AIzaSyCI1yfzpwm5-q-Qf11gPOOHd21PP0108CY";
    const coord1 = lat1 + "," + lon1;
    const coord2 = lat2 + "," + lon2;
    const googleURL = "https://maps.googleapis.com/maps/api/directions/json?origin=" + coord1 + "&destination=" + coord2 + "&key=" + APIKey;
    const body = await request(googleURL);
    const jsonData = JSON.parse(body);
    let seconds = jsonData.routes[0].legs[0].duration.value;
    let hours = seconds / 3600;
    hours = ~~hours;
    let remainder = seconds % 3600;
    let minutes = remainder / 60;
    minutes = ~~minutes;
    seconds = remainder % 60;
    seconds = ~~seconds;
    return {'hours': hours, 'minutes': minutes, 'seconds': seconds};
}

async function requestDriver(requestId, driver, message) {
    console.log("pusing", message);
    pubnub.publish({
        channel   : 'driverRequest',
        message   : message,
        callback  : function(e) {
            console.log( "SUCCESS!", e );
        },
        error     : function(e) {
            console.log( "FAILED! RETRY PUBLISH!", e );
        }
    });
    let request = await DriveRequest.findById(requestId);
    request.driver = driver.UUID;
    await request.save();

    for(let i=0; i<15; i++) {
        request = await DriveRequest.findById(requestId);
        if(request.responded) {
            if(request.accepted) return true;
            request.responded = false;
            await request.save();
            return false;
        }
        await P.delay(1000);
    }


    return false;
}

async function requestLoop(child, drivers, toLat, toLon) {
    let request = new DriveRequest({
        child: child.UUID,
        lat: toLat,
        lon: toLon,
    });
    await request.save();

    for (const driver of drivers) {
        const message = {
            request: request._id,
            driver: driver.UUID,
            child: child.UUID,
            toLat,
            toLon
        };

        if(await requestDriver(request._id, driver, message)) {
            request = await DriveRequest.findById(request._id);
            pubnub.publish({
                channel   : 'parentResponse',
                message   : {
                    driver: request.driver,
                    child: child.UUID,
                    toLat,
                    toLon
                },
                callback  : function(e) {
                    console.log( "SUCCESS!", e );
                },
                error     : function(e) {
                    console.log( "FAILED! RETRY PUBLISH!", e );
                }
            });
            return;
        }
    }
}

pubnub.subscribe({
    channel  : "driverResponse",
    message : async function(msg) {
        try {
            console.log( " > ", msg );
            //const msg = JSON.parse(message);

            const request = await DriveRequest.findById(msg.request);
            request.accepted = msg.accepted;
            request.responded = true;
            await request.save();
        } catch (err) {
            console.log(err);
        }
    }
});

export async function rideRequest(req, res, next){
    const {parentId, childId, toLat, toLon} = req.query;
    const child = await Child.findOne({UUID: childId});
    if(!child) {
        return res.json({success: false, message: "Can not find child with childId"});
    }

    const drivers = await Driver.find({city: child.city}).lean();
    for(const driver of drivers) {
        driver.eta = await geteta(child.lat, child.lon, driver.lat, driver.lon);
    }

    requestLoop(child, _.sortBy(drivers, function(driver) {
        return driver.eta.hours * 3600 + driver.eta.minutes * 60 + driver.eta.seconds;
    }), toLat, toLon);

    res.json(_.sortBy(drivers, function(driver) {
        return driver.eta.hours * 3600 + driver.eta.minutes * 60 + driver.eta.seconds;
    }));
}

