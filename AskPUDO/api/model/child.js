import mongoose, { Schema } from 'mongoose';

const Child = new Schema({
  gender: String,
  password: String,
  address: String,
  dob: String,
  UUID: String,
  username: String,
  fullname: String,
  lat: Number,
  lon: Number,
  city: String
}, { collection: 'users_child' });


export default mongoose.model('child', Child);
