import mongoose, { Schema } from 'mongoose';

const Driver = new Schema({
  gender: String,
  password: String,
  address: String,
  dob: String,
  UUID: String,
  username: String,
  fullname: String,
  plate: String,
  color: String,
  type: String,
  lat: Number,
  lon: Number,
  city: String
}, { collection: 'users_driver' });


export default mongoose.model('driver', Driver);
