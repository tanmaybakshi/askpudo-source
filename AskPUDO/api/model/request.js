import mongoose, { Schema } from 'mongoose';

const DriveRequest = new Schema({
  child: String,
  driver: String,
  lat: Number,
  lon: Number,
  accepted: {
    type: Boolean,
    default: false
  },
  responded: {
    type: Boolean,
    default: false
  },
  requestSentAt: {
    type: Date,
    default: Date.now
  }
});


export default mongoose.model('driverequest', DriveRequest);
