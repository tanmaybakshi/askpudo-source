import * as rideController from "../api/controllers/ride";


module.exports = function ( app ){

    let router = require("express-promise-router")();
    router.get("/rideRequest", rideController.rideRequest);

    app.use('/api/', router);
}
