import Parse from 'parse/node';
import config from 'config3';

const pubnub = require("pubnub")({
    publish_key   : config.pubnub.publish_key,
    subscribe_key : config.pubnub.subscribe_key
});

pubnub.publish({
    channel   : 'driverResponse',
    message   : {
        'driver': 'sdfadsfasdf',
        'request': process.argv[2],
        'accepted': true
    },
    callback  : function(e) {
        console.log( "SUCCESS!", e );
    },
    error     : function(e) {
        console.log( "FAILED! RETRY PUBLISH!", e );
    }
});
