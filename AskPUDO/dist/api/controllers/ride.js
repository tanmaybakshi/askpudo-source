'use strict';

var _regeneratorRuntime = require('babel-runtime/regenerator')['default'];

var _getIterator = require('babel-runtime/core-js/get-iterator')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
    value: true
});
exports.rideRequest = rideRequest;

var _parseNode = require('parse/node');

var _parseNode2 = _interopRequireDefault(_parseNode);

var _modelIndex = require('../model/index');

var _requestPromise = require("request-promise");

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _config3 = require('config3');

var _config32 = _interopRequireDefault(_config3);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var pubnub = require("pubnub")({
    publish_key: _config32['default'].pubnub.publish_key,
    subscribe_key: _config32['default'].pubnub.subscribe_key
});

function geteta(lat1, lon1, lat2, lon2) {
    var APIKey, coord1, coord2, googleURL, body, jsonData, seconds, hours, remainder, minutes;
    return _regeneratorRuntime.async(function geteta$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                APIKey = "AIzaSyCI1yfzpwm5-q-Qf11gPOOHd21PP0108CY";
                coord1 = lat1 + "," + lon1;
                coord2 = lat2 + "," + lon2;
                googleURL = "https://maps.googleapis.com/maps/api/directions/json?origin=" + coord1 + "&destination=" + coord2 + "&key=" + APIKey;
                context$1$0.next = 6;
                return _regeneratorRuntime.awrap((0, _requestPromise2['default'])(googleURL));

            case 6:
                body = context$1$0.sent;
                jsonData = JSON.parse(body);
                seconds = jsonData.routes[0].legs[0].duration.value;
                hours = seconds / 3600;

                hours = ~ ~hours;
                remainder = seconds % 3600;
                minutes = remainder / 60;

                minutes = ~ ~minutes;
                seconds = remainder % 60;
                seconds = ~ ~seconds;
                return context$1$0.abrupt('return', { 'hours': hours, 'minutes': minutes, 'seconds': seconds });

            case 17:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function requestDriver(requestId, driver, message) {
    var request, i;
    return _regeneratorRuntime.async(function requestDriver$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                console.log("pusing", message);
                pubnub.publish({
                    channel: 'driverRequest',
                    message: message,
                    callback: function callback(e) {
                        console.log("SUCCESS!", e);
                    },
                    error: function error(e) {
                        console.log("FAILED! RETRY PUBLISH!", e);
                    }
                });
                context$1$0.next = 4;
                return _regeneratorRuntime.awrap(_modelIndex.DriveRequest.findById(requestId));

            case 4:
                request = context$1$0.sent;

                request.driver = driver.UUID;
                context$1$0.next = 8;
                return _regeneratorRuntime.awrap(request.save());

            case 8:
                i = 0;

            case 9:
                if (!(i < 15)) {
                    context$1$0.next = 25;
                    break;
                }

                context$1$0.next = 12;
                return _regeneratorRuntime.awrap(_modelIndex.DriveRequest.findById(requestId));

            case 12:
                request = context$1$0.sent;

                if (!request.responded) {
                    context$1$0.next = 20;
                    break;
                }

                if (!request.accepted) {
                    context$1$0.next = 16;
                    break;
                }

                return context$1$0.abrupt('return', true);

            case 16:
                request.responded = false;
                context$1$0.next = 19;
                return _regeneratorRuntime.awrap(request.save());

            case 19:
                return context$1$0.abrupt('return', false);

            case 20:
                context$1$0.next = 22;
                return _regeneratorRuntime.awrap(_bluebird2['default'].delay(1000));

            case 22:
                i++;
                context$1$0.next = 9;
                break;

            case 25:
                return context$1$0.abrupt('return', false);

            case 26:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function requestLoop(child, drivers, toLat, toLon) {
    var request, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, driver, message;

    return _regeneratorRuntime.async(function requestLoop$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                request = new _modelIndex.DriveRequest({
                    child: child.UUID,
                    lat: toLat,
                    lon: toLon
                });
                context$1$0.next = 3;
                return _regeneratorRuntime.awrap(request.save());

            case 3:
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                context$1$0.prev = 6;
                _iterator = _getIterator(drivers);

            case 8:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                    context$1$0.next = 22;
                    break;
                }

                driver = _step.value;
                message = {
                    request: request._id,
                    driver: driver.UUID,
                    child: child.UUID,
                    toLat: toLat,
                    toLon: toLon
                };
                context$1$0.next = 13;
                return _regeneratorRuntime.awrap(requestDriver(request._id, driver, message));

            case 13:
                if (!context$1$0.sent) {
                    context$1$0.next = 19;
                    break;
                }

                context$1$0.next = 16;
                return _regeneratorRuntime.awrap(_modelIndex.DriveRequest.findById(request._id));

            case 16:
                request = context$1$0.sent;

                pubnub.publish({
                    channel: 'parentResponse',
                    message: {
                        driver: request.driver,
                        child: child.UUID,
                        toLat: toLat,
                        toLon: toLon
                    },
                    callback: function callback(e) {
                        console.log("SUCCESS!", e);
                    },
                    error: function error(e) {
                        console.log("FAILED! RETRY PUBLISH!", e);
                    }
                });
                return context$1$0.abrupt('return');

            case 19:
                _iteratorNormalCompletion = true;
                context$1$0.next = 8;
                break;

            case 22:
                context$1$0.next = 28;
                break;

            case 24:
                context$1$0.prev = 24;
                context$1$0.t0 = context$1$0['catch'](6);
                _didIteratorError = true;
                _iteratorError = context$1$0.t0;

            case 28:
                context$1$0.prev = 28;
                context$1$0.prev = 29;

                if (!_iteratorNormalCompletion && _iterator['return']) {
                    _iterator['return']();
                }

            case 31:
                context$1$0.prev = 31;

                if (!_didIteratorError) {
                    context$1$0.next = 34;
                    break;
                }

                throw _iteratorError;

            case 34:
                return context$1$0.finish(31);

            case 35:
                return context$1$0.finish(28);

            case 36:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this, [[6, 24, 28, 36], [29,, 31, 35]]);
}

pubnub.subscribe({
    channel: "driverResponse",
    message: function message(msg) {
        var _request;

        return _regeneratorRuntime.async(function message$(context$1$0) {
            while (1) switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    context$1$0.prev = 0;

                    console.log(" > ", msg);
                    //const msg = JSON.parse(message);

                    context$1$0.next = 4;
                    return _regeneratorRuntime.awrap(_modelIndex.DriveRequest.findById(msg.request));

                case 4:
                    _request = context$1$0.sent;

                    _request.accepted = msg.accepted;
                    _request.responded = true;
                    context$1$0.next = 9;
                    return _regeneratorRuntime.awrap(_request.save());

                case 9:
                    context$1$0.next = 14;
                    break;

                case 11:
                    context$1$0.prev = 11;
                    context$1$0.t0 = context$1$0['catch'](0);

                    console.log(context$1$0.t0);

                case 14:
                case 'end':
                    return context$1$0.stop();
            }
        }, null, this, [[0, 11]]);
    }
});

function rideRequest(req, res, next) {
    var _req$query, parentId, childId, toLat, toLon, child, drivers, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, driver;

    return _regeneratorRuntime.async(function rideRequest$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                _req$query = req.query;
                parentId = _req$query.parentId;
                childId = _req$query.childId;
                toLat = _req$query.toLat;
                toLon = _req$query.toLon;
                context$1$0.next = 7;
                return _regeneratorRuntime.awrap(_modelIndex.Child.findOne({ UUID: childId }));

            case 7:
                child = context$1$0.sent;

                if (child) {
                    context$1$0.next = 10;
                    break;
                }

                return context$1$0.abrupt('return', res.json({ success: false, message: "Can not find child with childId" }));

            case 10:
                context$1$0.next = 12;
                return _regeneratorRuntime.awrap(_modelIndex.Driver.find({ city: child.city }).lean());

            case 12:
                drivers = context$1$0.sent;
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                context$1$0.prev = 16;
                _iterator2 = _getIterator(drivers);

            case 18:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                    context$1$0.next = 26;
                    break;
                }

                driver = _step2.value;
                context$1$0.next = 22;
                return _regeneratorRuntime.awrap(geteta(child.lat, child.lon, driver.lat, driver.lon));

            case 22:
                driver.eta = context$1$0.sent;

            case 23:
                _iteratorNormalCompletion2 = true;
                context$1$0.next = 18;
                break;

            case 26:
                context$1$0.next = 32;
                break;

            case 28:
                context$1$0.prev = 28;
                context$1$0.t0 = context$1$0['catch'](16);
                _didIteratorError2 = true;
                _iteratorError2 = context$1$0.t0;

            case 32:
                context$1$0.prev = 32;
                context$1$0.prev = 33;

                if (!_iteratorNormalCompletion2 && _iterator2['return']) {
                    _iterator2['return']();
                }

            case 35:
                context$1$0.prev = 35;

                if (!_didIteratorError2) {
                    context$1$0.next = 38;
                    break;
                }

                throw _iteratorError2;

            case 38:
                return context$1$0.finish(35);

            case 39:
                return context$1$0.finish(32);

            case 40:

                requestLoop(child, _lodash2['default'].sortBy(drivers, function (driver) {
                    return driver.eta.hours * 3600 + driver.eta.minutes * 60 + driver.eta.seconds;
                }), toLat, toLon);

                res.json(_lodash2['default'].sortBy(drivers, function (driver) {
                    return driver.eta.hours * 3600 + driver.eta.minutes * 60 + driver.eta.seconds;
                }));

            case 42:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this, [[16, 28, 32, 40], [33,, 35, 39]]);
}