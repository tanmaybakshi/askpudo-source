'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var Driver = new _mongoose.Schema({
  gender: String,
  password: String,
  address: String,
  dob: String,
  UUID: String,
  username: String,
  fullname: String,
  plate: String,
  color: String,
  type: String,
  lat: Number,
  lon: Number,
  city: String
}, { collection: 'users_driver' });

exports['default'] = _mongoose2['default'].model('driver', Driver);
module.exports = exports['default'];