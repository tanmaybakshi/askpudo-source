'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var DriveRequest = new _mongoose.Schema({
  child: String,
  driver: String,
  lat: Number,
  lon: Number,
  accepted: {
    type: Boolean,
    'default': false
  },
  responded: {
    type: Boolean,
    'default': false
  },
  requestSentAt: {
    type: Date,
    'default': Date.now
  }
});

exports['default'] = _mongoose2['default'].model('driverequest', DriveRequest);
module.exports = exports['default'];