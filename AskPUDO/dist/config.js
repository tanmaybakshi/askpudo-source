/* do NOT change this to ES6 or config wont work */
"use strict";

var env = process.env;

module.exports = {
  MONGO_URI: env.MONGO_URI
};