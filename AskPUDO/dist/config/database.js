'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _config3 = require('config3');

var _config32 = _interopRequireDefault(_config3);

_mongoose2['default'].Promise = require('bluebird');

_mongoose2['default'].connect(_config32['default'].MONGO_URI);

_mongoose2['default'].connection.on('connected', function () {
  console.log('Mongoose connection open on ' + _config32['default'].MONGO_URI);
});

_mongoose2['default'].connection.on('error', function (err) {
  console.error('Mongoose connection error: ' + err);
});

_mongoose2['default'].connection.on('disconnected', function () {
  console.error('Mongoose connection disconnected');
});

process.on('SIGINT', function () {
  _mongoose2['default'].connection.close(function () {
    console.log('Mongoose connection disconnected through app termination');
    process.exit(0);
  });
});

module.exports = _mongoose2['default'].connection;