"use strict";

var _interopRequireWildcard = require("babel-runtime/helpers/interop-require-wildcard")["default"];

var _apiControllersRide = require("../api/controllers/ride");

var rideController = _interopRequireWildcard(_apiControllersRide);

module.exports = function (app) {

    var router = require("express-promise-router")();
    router.get("/rideRequest", rideController.rideRequest);

    app.use('/api/', router);
};