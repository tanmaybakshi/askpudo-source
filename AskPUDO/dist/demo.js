'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _parseNode = require('parse/node');

var _parseNode2 = _interopRequireDefault(_parseNode);

var _config3 = require('config3');

var _config32 = _interopRequireDefault(_config3);

var pubnub = require("pubnub")({
    publish_key: _config32['default'].pubnub.publish_key,
    subscribe_key: _config32['default'].pubnub.subscribe_key
});

pubnub.publish({
    channel: 'driverResponse',
    message: {
        'driver': 'sdfadsfasdf',
        'request': process.argv[2],
        'accepted': true
    },
    callback: function callback(e) {
        console.log("SUCCESS!", e);
    },
    error: function error(e) {
        console.log("FAILED! RETRY PUBLISH!", e);
    }
});