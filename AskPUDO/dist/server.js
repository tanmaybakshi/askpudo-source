'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _configDatabase = require('./config/database');

var _configDatabase2 = _interopRequireDefault(_configDatabase);

var app = (0, _express2['default'])();
app.use((0, _morgan2['default'])('short'));
app.use(_bodyParser2['default'].urlencoded({ extended: true }));
app.use(_bodyParser2['default'].json());

// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

require('./config/routes')(app);

app.use('', function (req, res) {
    res.json({ message: "undefined api" });
});

app.listen(process.env.PORT || 3000);

if (process.env.PORT === undefined) {
    console.log("Server Started at port : " + 3000);
} else {
    console.log("Server Started at port : " + process.env.PORT);
}